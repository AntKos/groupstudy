# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 23:10:18 2017

@author: Алия
"""

import numpy as np
import matplotlib.pyplot as plt
import copy
from optimizers.BaseOptimizer import BaseOptimizer

class CoordinateDescent(BaseOptimizer):
    
    def __init__(self, A, y, alpha=0.1, mu=0.1):
        self.PHI = (np.sqrt(5) +1)/2
                 
        BaseOptimizer.__init__(self, A, y, alpha, mu)

        
    def sd(self, i):
        e = 0.0005
        x_prev = self.beta[i] + 2*e
        x_new = self.beta[i]
        lr = 0.001
        ro = 0.0001
        q = 0.5
        
        while abs(x_new - x_prev) > e:
            x_prev = x_new
            
            x = self.beta
            x[i] = x_prev
            
            grad = self.gradient(x, self.A, self.A_t, self.y)
        
            while self.func(x + lr * (- grad) ) > self.func(x) + ro * lr* (np.dot(np.transpose(grad),grad)):
                lr = q * lr
            
            x_new += - lr * self.gradient(x, self.A, self.A_t, self.y)[i]
        self.beta[i] = x_new
        #print ("минимум на точке".format(x_new))
        
    def optimize(self, epochs=10):
        ''' runs optimization.
            Params:
                lr: learning rate constant
                momentum: another parameter
                epoch: how many times we will optimize parameters over the whole data'''
        max_epoch = 1e+5
        eps = 1e-5
        ro = 1e-4
        q = 1./2
        epoch = 0
       
        gradients = [ ]
        gradients.append(self.gradient(self.beta, self.A, self.A_t, self.y))
        losses = []
        losses.append(self.loss())
        betas = []
        betas.append(copy.copy(self.beta))
        terminated = False
        
        while not terminated:
    
            epoch = epoch + 1
        
            for j in range(0, len(self.beta)): 
                self.sd(j)
                betas.append(copy.copy(self.beta))
                losses.append(self.loss())
            
            betas.append(copy.copy(self.beta))
            
            if epoch > max_epoch:
                terminated = True
                print("terminated at step {} because of many epochs".format(epoch))
            elif np.linalg.norm(betas[epoch] - betas[epoch-1]) < eps:
                terminated = True
                print("terminated at step {} because of x didnt change a lot, epoch = {}".format(epoch, epoch))
            elif np.linalg.norm(self.gradient(self.beta, self.A, self.A_t, self.y)) < eps:
                terminated = True
                print("terminated at step {} because gradient is nearly zero, epoch = {}".format(epoch, epoch))
            
        return losses, epoch
