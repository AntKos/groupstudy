from __future__ import division
import numpy as np
import datetime as dt

class BaseOptimizer():
    
    def __init__(self, A, y, alpha=0, mu=0.01):
        self.A = np.matrix(A)
        print("A has shape {}".format(np.shape(self.A)))
        self.A_t = np.transpose(A)
        y_mat = np.matrix(y)
        y_shape = np.shape(y_mat)
        if y_shape[0] == 1:
            self.y = np.transpose(y_mat)
        else:
            self.y = y_mat
        
        #now = dt.datetime.now()
        #seed = now.microsecond
        seed = 10
        np.random.seed(seed)
        self.beta = np.random.rand(np.shape(A)[1], 1)
    #    print("constructed beta has shape {}".format(self.beta.shape))
        self.alpha = alpha
        self.mu = mu
        
    def loss(self):
        dp = np.dot(self.A, self.beta)
        tmp = self.y - dp
        root = np.sqrt(np.multiply(tmp, tmp))
        summ = np.sum(root)
        sse = summ / np.shape(self.A)[0]
        
        return sse

        #return np.linalg.norm(self.func(self.beta))
    
    def predict(self, data):
        return np.dot(data, self.beta)
    
    def func(self, beta):
        part11 = np.dot(self.A, beta)
        
        part12 = self.y - part11
        
        part13 = np.linalg.norm(part12, ord=2)
        
        part14 = np.multiply(part13, part13)
        
        part1 = part14*(1/2)
        
        part21 = np.linalg.norm(beta, ord=1)
        
        part2 = self.alpha * part21
  #      print("compute function, out shape = {}".format(np.shape(part1 + part2)))
        return part1 + part2
            
  
    def gradient(self, beta, A, A_t, y):
        '''Compute gradient of target function in point beta'''
    #    print("compute gradient with shapes {0}, {1}, {2}, {3}".format(np.shape(beta), np.shape(A), np.shape(A_t), np.shape(y) ))
 #       print("A * beta shape = {}".format(np.shape(np.dot(A, beta))))
 #       print("y shape = {}".format(np.shape(y)))
        part11 = y - np.dot(A, beta)
 #       print("part11 shape = {}".format(np.shape(part11)))
        part1 = -2 * np.dot(A_t, part11)
 #       print("part1 shape = {}".format(np.shape(part1)))
        
        part21 = self.alpha * self.mu * np.sqrt( self.mu**2 / (np.sum(np.multiply(beta, beta)) + self.mu**2))
 #       print("part21 shape = {}".format(np.shape(part21)))
        part22 = 2 * np.sum(np.multiply(beta, beta))
#       print("part22 shape = {}".format(np.shape(part22)))
#        print("result shape = {}".format(np.shape(part1 + part21 * part22)))
        return part1 + part21 * part22
    
    def func_part(self, beta, A, y): 
        part11 = np.dot(A, beta) 
        
        part12 = y - part11 

        part13 = np.linalg.norm(part12, ord=2) 

        part14 = np.multiply(part13, part13)

        part1 = part14*(1/2) 

        part21 = np.linalg.norm(beta, ord=1) 

        part2 = self.alpha * part21 
  #      print("functtion  returns shape {}".format(np.shape(part1 + part2)))
        return part1 + part2   
   
    def gradient_local(self, beta, A, y):
        '''Compute gradient of target function in point beta'''
        part11 = beta * np.dot(A, A)
        part12 = np.sum(A * y)
        
        part1 = part11 - part12
        
        part21 = self.alpha * self.mu * np.sqrt( self.mu**2 / (np.sum(np.multiply(beta, beta)) + self.mu**2))
        part22 = 2 * np.sum(np.multiply(beta, beta))
        return part1 + part21 * part22

    def gradient_batch(self, beta, A, A_t, y):
        '''Compute gradient of target function in point beta'''
        part11 = np.dot(np.dot(A_t, A), beta)
        part12 = np.dot(A_t, y)
        
        part1 = part11 - part12
        
        part21 = self.alpha * self.mu * np.sqrt( self.mu**2 / (np.sum(np.multiply(beta, beta)) + self.mu**2))
        part22 = 2 * np.sum(np.multiply(beta, beta))
        return part1 + part21 * part22
    