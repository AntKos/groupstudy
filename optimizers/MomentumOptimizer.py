import numpy as np
from optimizers.BaseOptimizer import BaseOptimizer

class MomentumOptimizer(BaseOptimizer):
    
    def __init__(self, A, y, alpha=0.1, mu=0.1):
        BaseOptimizer.__init__(self, A, y, alpha, mu)
        
    def optimize(self, lr=0.001, momentum_fraction=0.9, max_epoch=1e+5):
        ''' runs optimization.
            Params:
                lr: learning rate constant
                momentum: another parameter'''
        #termination constants
        max_epoch = max_epoch
        eps = 1e-1
        #armijo rule constants
        ro = 1e-4
        q = 1./2
        
        
        epoch = 0
        vp = 0
        gradients = [ ]
        gradients.append(self.gradient(self.beta, self.A, self.A_t, self.y))
        losses = [ ]
        losses.append(self.loss())
        betas = [ ]
        betas.append(self.beta)
        terminated = False
        
        while not terminated:
            
            epoch = epoch + 1 
            
            momentum = momentum_fraction * vp 
            gradient = self.gradient(self.beta, self.A, self.A_t, self.y)
            
            gradient_t = np.transpose(gradient)
            
            #armijo rule for step-length
            alpha = 1
            while self.func(self.beta - alpha * gradient) > self.func(self.beta) + ro * alpha * (np.dot(gradient_t, gradient)):
                alpha = alpha * q  
            
            vp = momentum + alpha * gradient
            self.beta = self.beta - vp
            
            losses.append(self.loss())
            gradients.append(gradient)
            betas.append(self.beta)
            
            #check termination conditions:
            if epoch > max_epoch:
                terminated = True
                print("terminated at step {} because of many epochs".format(epoch))
            elif np.linalg.norm(betas[epoch] - betas[epoch-1]) < eps:
                terminated = True
                print("terminated at step {} because of x didnt change a lot, epoch = {}".format(epoch, epoch))
            elif np.linalg.norm(gradient) < eps:
                terminated = True
                print("terminated at step {} because gradient is nearly zero, epoch = {}".format(epoch, epoch))
             
        
        return gradients, losses, epoch, betas