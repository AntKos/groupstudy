import numpy as np
import BaseOptimizer; reload(BaseOptimizer)
from BaseOptimizer import BaseOptimizer

class NesterovOptimizer(BaseOptimizer):
    
    def __init__(self, A, y, alpha=0.1, mu=0.1):
        BaseOptimizer.__init__(self, A, y, alpha, mu)
        
    def optimize(self, momentum_fraction=0.9, max_epoch=1e+5):
        ''' runs optimization.
            for details see https://blogs.princeton.edu/imabandit/2013/04/01/acceleratedgradientdescent/
            Params:
                lr: learning rate constant
                momentum: another parameter'''
        
        
        #termination constants
        eps = 1e-5
        #armijo rule constants
        
        ro = 1e-4
        q = 1./2
        
        
        
        
        gradients = [ ]
        gradients.append(self.gradient(self.beta, self.A, self.A_t, self.y))
        losses = [ ]
        losses.append(self.func(self.beta))
        betas = [ ]
        betas.append(self.beta)
        lrs = []
        lrs.append(0)
        
        lbd0 = 0
        lbd1 = 0
        y0 = 0
        y1 = 0
        
        epoch = 0
        terminated = False
        while not terminated:
            
            epoch = epoch + 1 
            
            #compute lambda
            lbd1 = (1 + np.sqrt(1 + 4 * lbd0 * lbd0)) / 2
            #compute gamma
            gamma = (1. - lbd0 ) / lbd1
            
            gradient = self.gradient(self.beta, self.A, self.A_t, self.y)
            gradient_t = np.transpose(gradient)
            
            #armijo rule for step-length
            alpha = 1e-2
            while self.func(self.beta - alpha * gradient) > self.func(self.beta) + ro * alpha * (np.dot(gradient_t, gradient)):
                alpha = alpha * q                                                                                     
            
            lrs.append(alpha)
            y1 = self.beta - alpha * gradient
            self.beta = (1. - gamma) * y1 + gamma * y0
            
            
            lbd0 = lbd1
            y0 = y1
            
            losses.append(self.func(self.beta))
            gradients.append(gradient)
            betas.append(self.beta)
         #   print('betas = {}'.format(betas))
         #   print('gradient = {}'.format(gradients))
            
            #check termination conditions:
            if epoch > max_epoch:
                terminated = True
                print("terminated at step {} because of many epochs".format(epoch))
            elif np.linalg.norm(betas[epoch] - betas[epoch-1]) < eps:
                terminated = True
                print("terminated at step {} because of x didnt change a lot, epoch = {}".format(epoch, epoch))
            elif np.linalg.norm(gradient) < eps:
                terminated = True
                print("terminated at step {} because gradient is nearly zero, epoch = {}".format(epoch, epoch))
             
        
        return gradients, losses, epoch, lrs
