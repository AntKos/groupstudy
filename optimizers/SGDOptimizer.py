import numpy as np
import BaseOptimizer; reload(BaseOptimizer)
from BaseOptimizer import BaseOptimizer
import copy

class SGDOptimizer(BaseOptimizer):
    
    def __init__(self, A, y, alpha=0.1, mu=0.1):
        BaseOptimizer.__init__(self, A, y, alpha, mu)

    def ArmijoRule(self, a, g, g_t, ro, q):
        alpha = a
        while self.func(self.beta - alpha * g) > self.func(self.beta) + ro * alpha * (np.dot(g_t, g)):
            alpha = alpha * q
        return alpha

    def StaticDecreaseRule(self, a, decRate):
        return a - decRate;
        
    def optimize(self, max_epoch=1e+5, lr0 = (1e-3 + 1e-4)/4):
        #termination constants
        eps = 1e-5
        #armijo rule constants
        ro = 1e-4
        q = 1./2
        
        #Predefining parametrs
        gradients = [ ]
        #gradients.append(self.gradient_local(self.beta, self.A, self.y))
        losses = [ ]
        #losses.append(self.loss())
        betas = [ ]
        #betas.append(copy.copy(self.beta))
        lrs = []
        
        epoch = 0
        terminated = False

        length = len(self.y)

        decRate = 1e-7
        while not terminated:

            epoch = epoch + 1

            a = np.random.permutation(length)
            for j in range(length):
                example = self.A[a[j]]
                y = self.y[a[j]]

                gradient_i = self.gradient_local(self.beta, example, y)
                gradient_t = np.transpose(np.matrix(gradient_i))
                
                lr = lr0
                lr = self.ArmijoRule(a = lr, g = gradient_i, g_t = gradient_t, ro = ro, q = q)
                lrs.append(lr)

                self.beta = self.beta - lr * gradient_i

                print("Loss {} ; lr {} ;".format(self.loss(), lr))
            
                losses.append(self.loss())
                gradients.append(gradient_i)
                betas.append(copy.copy(self.beta))
            
                #check termination conditions:
                if epoch > max_epoch:
                    terminated = True
                    print("terminated at step {} because of many epochs".format(epoch))
                elif np.linalg.norm(gradient_i) < eps:
                    terminated = True
                    print("terminated at step {} because gradient is nearly zero, epoch = {}".format(j+1, epoch))
                elif lr < eps*eps:
                    terminated = True
                    print("terminated at step {} because step length is too small, epoch = {}".format(j+1, epoch))

                if terminated:
                    break;
        
        return gradients, losses, epoch, lrs, betas


    def batch(self, iterable1, iterable2, n=1):
        l = len(iterable1)
        for ndx in range(0, l, n):
            yield iterable1[ndx:min(ndx + n, l)], iterable2[ndx:min(ndx + n, l)]

    def optimizeBatch(self, max_epoch=1e+5, batch_size = 100, lr0 = (1e-3 + 1e-4)/4):
        #termination constants
        eps = 1e-5
        #armijo rule constants
        ro = 1e-4
        q = 1./2
        
        #Predefining parametrs
        gradients = [ ]
        #gradients.append(self.gradient_local(self.beta, self.A, self.y))
        losses = [ ]
        #losses.append(self.loss())
        betas = [ ]
        #betas.append(copy.copy(self.beta))
        lrs = []
        
        epoch = 0
        terminated = False

        length = len(self.y)

        decRate = 1e-7
        
        while not terminated:

            epoch = epoch + 1

            a = np.random.permutation(length)

            j = 0;

            for x, y in self.batch(self.A, self.y, batch_size):
                gradient_i = self.gradient_batch(self.beta, x, np.transpose(x), y)
                gradient_t = np.transpose(np.matrix(gradient_i))
                
                lr = lr0
                lr = self.ArmijoRule(a = lr, g = gradient_i, g_t = gradient_t, ro = ro, q = q)
                lrs.append(lr)

                self.beta = self.beta - lr * gradient_i

                #print("Loss {} ; lr {} ;".format(self.loss(), lr))
            
                losses.append(self.loss())
                gradients.append(gradient_i)
                betas.append(copy.copy(self.beta))
            
                j = j + 1
                #check termination conditions:
                if epoch > max_epoch:
                    terminated = True
                    print("terminated at step {} because of many epochs".format(epoch))
                elif np.linalg.norm(gradient_i) < eps:
                    terminated = True
                    print("terminated at step {} because gradient is nearly zero, epoch = {}".format(j+1, epoch))
                elif lr < eps*eps*eps*eps*eps*eps:
                    terminated = True
                    print("terminated at step {} because step length is too small, epoch = {}".format(j+1, epoch))

                if terminated:
                    break;

            #print("Loss {} ; lr {} ;".format(self.loss(), lr))

        return gradients, losses, epoch, lrs, betas
