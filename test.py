#converge process demo
from __future__ import division
import utils; reload(utils)
from utils import *
import matplotlib.pyplot as plt

features, targets = read_fars_dataset("./data/FARS.csv")#read_linear_dataset(15, 2)
A = centering(features)

import optimizers.NesterovOptimizer; reload(optimizers.NesterovOptimizer)
from optimizers.NesterovOptimizer import NesterovOptimizer
import optimizers.CoordinateDescent; reload(optimizers.CoordinateDescent)
from optimizers.CoordinateDescent import CoordinateDescent
import optimizers.SGDOptimizer; reload(optimizers.SGDOptimizer)
from optimizers.SGDOptimizer import SGDOptimizer


alpha = [1000]
for ind, a in enumerate(alpha):
    opt = NesterovOptimizer(A, targets, alpha=a, mu=0.5)

    gradients, losses, epoch, betas = opt.optimize(momentum_fraction=0.1, max_epoch=30)
    print("finish for alpha = {0}".format(a))
    
beta0 = np.zeros(len(betas))
beta1 = np.zeros(len(betas))
for i in range(len(betas)):
    beta0[i] = betas[i][0][0]
    beta1[i] = betas[i][1][0]
    
delta = 0.01
x1 = np.arange(-1, 5, delta)
x2 = np.arange(-1, 5, delta)
X1, X2 = np.meshgrid(x1, x2)

Z = np.zeros((len(x1), len(x1)))
for i in range(len(X1)):
    for j in range(len(X1[i])):
        Z[i][j] = opt.func([X1[i][j], X2[i][j]])
        
plt.figure()
#levels = np.arange(0, 1200, 50)
levels = [350, 375, 400, 450, 500, 600, 700, 1000, 1100]
CS = plt.contour(X1, X2, Z, levels = levels)
plt.plot(beta0, beta1, 'ro-')
plt.clabel(CS, inline=1, fontsize=10)
plt.title('NAG')
plt.savefig('imgs/nesterov_convergence.svg')