from __future__ import division
import pandas as pd
import numpy as np


def read_dataset(filepath):
    '''reads csv file with all features/predictions inside
        Params:
            filepath: path to a file'''
    train = pd.read_csv(filepath)
    
    target = train.columns.tolist()[-1:]
    feature = train.columns.tolist()[:-3]
    
    features = train[feature]
    targets = train[target]
    
    features.datetime = pd.to_datetime(features.datetime)
    features['hour'] = features.datetime.dt.hour
    features['day'] = features.datetime.dt.day
    features['dayofweek'] = features.datetime.dt.dayofweek
    features['month'] = features.datetime.dt.month
    features.drop('datetime', axis=1, inplace=True)
    return features, targets

def read_house_prices(filepath):
    '''reads csv file with all features/predictions inside
        Params:
            filepath: path to a file'''
    train = pd.read_csv(filepath)
    
    target = train.columns.tolist()[2]
    feature = train.columns.tolist()[1:]
    
    features = train[feature]
    targets = train[target]
    
    features.drop('price', axis=1, inplace=True)
    features.drop('date', axis=1, inplace=True)
    features.drop('yr_renovated', axis=1, inplace=True)
    return features, targets

def read_fars_dataset(filepath):
    '''reads csv file with all features/predictions inside
        Params:
            filepath: path to a file'''
    train = pd.read_csv(filepath)
    
    target = train.columns.tolist()[8]
    feature = train.columns.tolist()[2:]
    
    features = train[feature]
    targets = train[target]
    
    features.drop('inimpact', axis=1, inplace=True)
    return features, targets


def read_linear_dataset(num_rows = 1000, num_columns = 8):
    coeffs = [i * 2 for i in range(num_columns)]
    
    random_samples = np.random.rand(num_rows, num_columns)
    targets = np.reshape(np.dot(random_samples, coeffs), (random_samples.shape[0], 1))
    return random_samples, targets
    
def read_linear_dataset2(num_rows = 1000):
    coeffs = [2, -7, 0,  4, 0.1, 0.8, 45]
    num_columns = len(coeffs)
    
    noise = np.random.normal(0,1, (num_rows, num_columns))
    
    random_samples = np.random.rand(num_rows, num_columns)
    targets = np.reshape(np.dot(random_samples, coeffs), (random_samples.shape[0], 1))
    return random_samples + noise, targets

def read_kaggle_test(filepath, norm_columns=None, means=None, stds=None):
    ''' Reads test dataset, which could be submitted to Kaggle 
        Params:
            filepath: path to a file
            norm_columns: columns, at which we apply normalization
            means: means for specified columns
            stds: stds for specified columns'''
    
    
    
    
    features = pd.read_csv(filepath)
    
    features.datetime = pd.to_datetime(features.datetime)
    features['hour'] = features.datetime.dt.hour
    features['day'] = features.datetime.dt.day
    features['dayofweek'] = features.datetime.dt.dayofweek
    features['month'] = features.datetime.dt.month
    times = features['datetime']
    features.drop('datetime', axis=1, inplace=True)
    
    if norm_columns is not None:
        assert(len(norm_columns) == len(means) == len(stds))
        
        for idx, column in enumerate(norm_columns):
            features[column] = features[column] - means[idx]
            features[column] = features[column] / stds[idx]
        
    return features, times.tolist()

def read_test_dataset():
    ''' Makes simple noisy sinus dataset to quick evaluation of optimizers work'''
    x = np.linspace(-3,3,50)
    np.random.seed(10)  #Setting seed for reproducability
    y = np.sin(x)# + np.random.normal(0,0.15,len(x))
    x = np.reshape(x, (len(x), 1))
    return x, y

def make_submission(times, predictions, path):
    ''' Make csv file with predictions.
        See /data/sampleSubmission.csv for details
        Params:
            times: 1st column of predictions - formatted time string
            predictions: number of bycicles rent
            path: path to csv file'''
    
    assert(len(times) == len(predictions))
    #fix with positive numbers should be removed 
    csv_data = {'datetime': times,
                   'count': [ elem[0] if elem[0] > 0 else 0 for elem in predictions]}
    df = pd.DataFrame(csv_data)
    df.to_csv(path, index=False)  

def normalize(data, columns):
    """ Make data normalized by substracting mean and division by std
        each column
        @see http://cs231n.github.io/neural-networks-2/
        
        Params:
            data: pandas dataframe
            columns: list of columns to normalize
            
        Returns:
            means: list of means for each column specified
            stds: list of standart deviations for each column"""
            
    
    # mean substraction
    means = [data[column].mean() for column in columns]
    for idx, column in enumerate(columns):
        data[column] = data[column] - means[idx]
    
    #normalization
    
    stds = [data[column].std() for column in columns]
    for idx, column in enumerate(columns):
        data[column] = data[column] / stds[idx]
    
    print(means)
    print(stds)
    return means, stds
        
    
def centering(data):
    # mean substraction
    
    columns = np.shape(data)[1]
    
    for column in range(columns):
        mean =  data[column].mean()
        data[column] = data[column] - mean
        
    return data
    
def centering_df(data):
    # mean substraction
    
    columns = np.shape(data)[1]
    means = [data[column].mean() for column in data.columns]
    print("means = {}".format(means))
    for i, column in enumerate(data.columns):
        data[column] = data[column] - means[i]
        
    return data

def centering_tg(data):
    mean = data.mean()
    data = data - mean
    
    return data
    
    