#matplotlib inline
from __future__ import division
import utils; reload(utils)
from utils import *
import matplotlib.pyplot as plt

import optimizers.SGDOptimizer; reload(optimizers.SGDOptimizer)
from optimizers.SGDOptimizer import SGDOptimizer
#lbd = [0., 1., 2., 5., 10., 20., 25., 50.]
lbd = [10e-100, 10e-50, 10e-10, 10e0, 10e10, 10e50, 10e100, 10e500]
colors = ['r', 'g', 'b', 'lightsalmon', 'orange', 'aquamarine', 'hotpink', 'darkkhaki']


#see https://www.kaggle.com/c/bike-sharing-demand
path = './data/train.csv'

features, targets = read_fars_dataset("./data/FARS.csv")#read_linear_dataset(3000000, 7) #read_dataset(path) #
A = centering_df(features)
y = centering_tg(targets)
losses_array = []

for ind,l in enumerate(lbd):
    opt = SGDOptimizer(A, y, alpha=.001, mu=0.01)

    grads, losses, epochs, lrs, betas = opt.optimizeBatch(max_epoch=1000, batch_size=100000, lr0 = 1e-8)
    losses = np.matrix(losses)
    losses_array.append(losses)
    print("finish for lambda = {0}".format(l))
#    print("losses = {}".format(losses))

plt.figure()
for i in range(len(lbd)):
    x_test = np.linspace(0, len(losses_array[i].A1)-1, len(losses_array[i].A1))
    plt.plot(x_test[:], losses_array[i].A1[:], color=colors[i], label='lambda={}'.format(lbd[i]))
plt.xlabel('iteration')
plt.ylabel('objective function value')
plt.title('BGD performs on a dataset with different parameters')
plt.legend()
plt.show()




#from __future__ import division
#import utils; reload(utils)
#from utils import read_dataset, read_linear_dataset, read_test_dataset, read_fars_dataset
#import matplotlib.pyplot as plt
#import numpy as np

##see https://www.kaggle.com/c/bike-sharing-demand
#path = './data/train.csv'

##make fake_data=True to use sin(x) daplotlytaset
#fake_data = False

#if fake_data:
#    features, targets = read_linear_dataset(50, 5)
#    A = features
#else:
#    features, targets = read_dataset(path)
#    features.head()
#    A = features.as_matrix()

#print np.shape(A)
#print np.shape(targets)

#import optimizers.SGDOptimizer; reload(optimizers.SGDOptimizer)
#from optimizers.SGDOptimizer import SGDOptimizer

#opt = SGDOptimizer(A, targets, alpha=.01, mu=0.05)
#gradients, losses, epochs, lrs, betas = opt.optimize(20)

#x_test = np.linspace(0, epochs * np.alen(losses), np.alen(losses))
#beta0 = np.zeros(len(betas))
#beta1 = np.zeros(len(betas))
#for i in range(len(betas)):
#    beta0[i] = betas[i][0][0]
#    beta1[i] = betas[i][1][0]

#plt.plot(x_test, losses, '-r')
#plt.title('losses')
#plt.show()


